#!/bin/sh

set -e

(cd debian/tests/files && python3 -m http.server 8080 2>/dev/null &)

parsero -u http://127.0.0.1:8080 | grep "http://127.0.0.1:8080/test 200 OK"
